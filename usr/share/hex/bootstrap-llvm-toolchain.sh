#!/bin/bash

set -e

if [[ ${#} -lt 2 ]]; then
    echo "USAGE: ${0} <host> <slot>"
    exit 1
fi

HOST=${1}
LLVM_SLOT=${2}
BOOTSTRAP_SLOT=$(expr ${LLVM_SLOT} - 1)
NEXT_SLOT=$(expr ${LLVM_SLOT} + 1)

if [[ ${HOST} == x86_64-*-*-gnu ]]; then
    PROFILE=amd64/clang
elif [[ ${HOST} == x86_64-*-*-musl ]]; then
    PROFILE=amd64/musl/clang
elif [[ ${HOST} == aarch64-*-*-gnueabi ]]; then
    PROFILE=armv8/linux/gnueabi/clang
else
    echo "ERROR: Could not determine profile for host"
    exit 2
fi

echo "Going to bootstrap LLVM toolchain"
echo "  Host: ${HOST}"
echo "  Profile: ${PROFILE}"
echo "  LLVM slot: ${LLVM_SLOT}"
echo "  Bootstrap LLVM slot: ${BOOTSTRAP_SLOT}"
echo ""

edo() {
    echo -e "\n# ${@}\n" 1>&2
    "${@}" || exit 123
}

edo cave sync

mkdir -v /etc/paludis/options.conf.d
mkdir -v /etc/paludis/package_mask.conf.d
mkdir -v /etc/paludis/package_unmask.conf.d

# Unmask bootstrap slot in case we're using a testing version for bootstrapping
cat <<EOF >> /etc/paludis/package_unmask.conf.d/clang-bootstrap.conf
dev-lang/llvm:${BOOTSTRAP_SLOT}
dev-lang/clang:${BOOTSTRAP_SLOT}
dev-libs/compiler-rt:${BOOTSTRAP_SLOT}
sys-libs/libc++abi[>=${BOOTSTRAP_SLOT}&<${LLVM_SLOT}]
sys-libs/libc++[>=${BOOTSTRAP_SLOT}&<${LLVM_SLOT}]
sys-libs/llvm-libunwind[>=${BOOTSTRAP_SLOT}&<${LLVM_SLOT}]
EOF

# Need to mask newer versions of libc++{,abi} during bootstrap to avoid
# llvm:${LLVM_SLOT}
# Need to mask compiler-rt:${LLVM_SLOT} to avoid llvm-libunwind pulling it in
cat <<EOF >> /etc/paludis/package_mask.conf.d/clang-bootstrap.conf
dev-lang/llvm:${LLVM_SLOT}
dev-lang/clang:${LLVM_SLOT}
dev-libs/compiler-rt:${LLVM_SLOT}
sys-libs/libc++abi[>=${LLVM_SLOT}]
sys-libs/libc++[>=${LLVM_SLOT}]
sys-libs/llvm-libunwind[>=${LLVM_SLOT}]
EOF

# Disable all tests during bootstrapping
cat <<EOF >> /etc/paludis/options.conf.d/no-tests.conf
*/* build_options: -recommended_tests
EOF

# Bootstrap process using clang-${BOOTSTRAP_SLOT}:
# - Build clang-${BOOTSTRAP_SLOT}[providers:compiler-rt][providers:libc++] using GCC
#   - We need to build libc++[providers:compiler-rt] with
#     clang[providers:compiler-rt], so avoid it in the resolution of
#     clang:${BOOTSTRAP_SLOT}
#   - eclectic-clang depends on clang:* which would select the best available
#     clang, don't take that dependency
#
# - Build libc++[providers:libgcc] + libc++abi[providers:libgcc]
#   - We have to build libc++[compiler-rt] with clang, but that requires us to
#     have libc++ installed already as it otherwise fails to link against -lc++
#   - We don't want to build llvm:${LLVM_SLOT} yet, so we have to pass
#     `--slots installed-or-best` to make the installed llvm:${BOOTSTRAP_SLOT}
#     satisfy libc++'s dependency on `llvm:*`
#
# - Switch to clang:${BOOTSTRAP_SLOT}[providers:compiler-rt][providers:libc++]
#   as system compiler
# - Switch to clang profile
# - Build libc++[providers:compiler-rt] + libc++abi[providers:compiler-rt]
#   - Now that we have libc++ (linking against libgcc) we can recompile it using
#     the bootstrap clang to have it link against compiler-rt
# - Build clang-${LLVM_SLOT} using clang-${BOOTSTRAP_SLOT} to have clang+llvm
#   linking against libc++/compiler-rt

# Assumptions about system state:
# - full LLVM toolchain already installed in normal configuration
#
# The base image provides a GCC-built LLVM/Clang toolchain, we only need to
# install llvm-libunwind
edo cave resolve -1z llvm-libunwind -x

edo sed -e "/^profiles =/s:\(profiles\)/.*:\1/${PROFILE}:" \
        -i /etc/paludis/repositories/arbor.conf
edo cave resolve \
        -1z -x \
        clang:${BOOTSTRAP_SLOT} \
        -0 eclectic-clang
edo eclectic cc set clang
edo eclectic c++ set clang
edo eclectic cpp set clang
edo eclectic clang set ${BOOTSTRAP_SLOT}

edo cave resolve \
        -1z -x \
        compiler-rt:${BOOTSTRAP_SLOT}

# Build libc++[providers:compiler-rt] + libc++abi[providers:compiler-rt]
edo cave resolve -1z -si libc++ libc++abi -x

# Remove bootstrap masks
edo rm -v /etc/paludis/package_mask.conf.d/clang-bootstrap.conf
edo rm -v /etc/paludis/package_unmask.conf.d/clang-bootstrap.conf

# Unmask final slot
cat <<EOF >> /etc/paludis/package_unmask.conf.d/clang.conf
dev-lang/llvm:${LLVM_SLOT} toolchain-dev testing scm
dev-lang/clang:${LLVM_SLOT} toolchain-dev testing scm
dev-libs/compiler-rt:${LLVM_SLOT} toolchain-dev testing scm
sys-devel/lld:${LLVM_SLOT} toolchain-dev testing scm

sys-libs/libc++ toolchain-dev testing scm
sys-libs/libc++abi toolchain-dev testing scm
sys-libs/llvm-libunwind toolchain-dev testing scm
sys-libs/openmp toolchain-dev testing scm
sys-devel/lldb toolchain-dev testing scm
dev-cpp/pstl toolchain-dev testing scm
EOF

# Mask newer versions of packages that aren't slotted
cat <<EOF >> /etc/paludis/package_mask.conf.d/clang.conf
sys-libs/libc++[>=${NEXT_SLOT}]
sys-libs/libc++abi[>=${NEXT_SLOT}]
sys-libs/llvm-libunwind[>=${NEXT_SLOT}]
sys-libs/openmp[>=${NEXT_SLOT}]
sys-devel/lldb[>=${NEXT_SLOT}]
dev-cpp/pstl[>=${NEXT_SLOT}]
EOF

# Build the complete final toolchain with the bootstrap toolchain
edo cave resolve -1z -x \
        llvm:${LLVM_SLOT} \
        clang:${LLVM_SLOT} \
        compiler-rt:${LLVM_SLOT} \
        lld:${LLVM_SLOT} \
        libc++ \
        libc++abi \
        llvm-libunwind

edo eclectic llvm set ${LLVM_SLOT}
edo eclectic clang set ${LLVM_SLOT}
edo eclectic ld set lld
edo eclectic lld set ${LLVM_SLOT}
edo cave uninstall -x \
        llvm:${BOOTSTRAP_SLOT} \
        clang:${BOOTSTRAP_SLOT} \
        compiler-rt:${BOOTSTRAP_SLOT} \
        lld:${BOOTSTRAP_SLOT} \
        -u clang \
        -u system

edo rm /etc/paludis/options.conf.d/no-tests.conf
