#!/bin/sh -ex

mkdir -p /etc/hex/keyring
gpg --no-default-keyring --keyring /etc/hex/keyring/pubring.kbx --fingerprint
gpg --no-default-keyring --keyring /etc/hex/keyring/pubring.kbx --import /usr/share/hex/hex.asc
trustfile=$(mktemp)
echo "5DF763560390A149AC6C14C7D076A377FB27DE70:5:" > "$trustfile"
gpg --no-default-keyring --keyring /etc/hex/keyring/pubring.kbx --import-ownertrust "$trustfile"
rm -f "$trustfile"
