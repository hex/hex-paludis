#!/bin/bash -ex

for p in do{ftp,http,https}; do
	ln -sfv /usr/share/paludis/fetchers/demos/docurl /usr/share/paludis/fetchers/$p
	ln -sfv /usr/share/paludis/fetchers/demos/docurl /usr/host/libexec/paludis/fetchers/$p
done

cave print-unused-distfiles | xargs rm -vf

ln -sfv /etc/rc.init /usr/host/bin/rc.init
ln -sfv /etc/rc.shutdown /usr/host/bin/rc.shutdown

find /var/log/paludis -type f -'(' -name '*.out' -o -name '*.messages' -')' -delete
rm -f /var/log/paludis.log
rm -rf /var/tmp/paludis/build/*

rm -f /root/.bash_history
rm -f /root/.lesshst
rm -f /root/.cave-resume
rm -f /root/.cave-unmanaged
