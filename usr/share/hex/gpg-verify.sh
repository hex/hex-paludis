#!/bin/sh -ex

exec gpg --no-default-keyring --keyring /usr/share/hex/hex.kbx --verify "$@"
